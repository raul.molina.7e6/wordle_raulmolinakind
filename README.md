# Wordle_RaulMolina
## Descripció
El joc d'endevinar paraules "wordle" fet amb kotlin i per jugar-ho a la consola d'IntelliJ IDEA

## Instal·lació
Per jugar hauràs de tenir instal·lat al teu pc l'IDE IntelliJ IDEA. A continuació t'has de descarregar aquest projecte des de la branca "ProjectConGradle" i obrir-lo des de l'IntelliJ. Executeu el "menu.kt" amb el botó d'execució.

## Intruccións del joc
Quan executis el joc et sortira un menu amb varies opcions, una d'aquestes es la de "tutorial". Aquesta opció t'explicara com jugar al wordle.

## Novetats
-Ara es pot jugar amb paraules amb lletres repetides.
-3 idiomes per jugar
-100 paraules per endevinar

## Llicència
MIT License

Copyright (c) 2022 Raul Molina Kind

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.