import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

internal class MainKtTest{
    //5 letters checker
    @Test
    fun wordWith5Letters(){
        assertFalse(not5Letters("abcde"))
    }
    @Test
    fun wordNotWith5Letters(){
        assertTrue(not5Letters("abc"))
    }
    @Test
    fun wordNotWith5Letters2(){
        assertTrue(not5Letters("abcdefg"))
    }
    //Not existing word
    @Test
    fun existingWord(){
        assertFalse(notExistingWord("MAGMA", "español"))
    }
    @Test
    fun notExistingWord(){
        assertTrue(notExistingWord("hello", "ingles"))
    }
    @Test
    fun notExistingWord2(){
        assertTrue(notExistingWord("ferir", "catalan"))
    }
}